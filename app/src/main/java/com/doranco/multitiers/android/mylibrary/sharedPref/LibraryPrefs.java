package com.doranco.multitiers.android.mylibrary.sharedPref;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 *
 * l'interface du nom d'utilisaeur logé qui sera transmis
 * @SharePref genere les methodes pour atteindre nos variables
 */
@SharedPref (SharedPref.Scope.UNIQUE)
//@SharedPref
public interface LibraryPrefs {

    // le nom de l'utilisateur
    // on a 3 methodes implicite : get(), put(), exist()
    String loggedUserName();
    String token();
    String serverMessage();

    long idUser();

}
