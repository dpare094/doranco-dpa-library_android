package com.doranco.multitiers.android.mylibrary.model;

import java.util.Date;

public class Note {


    private long idUser;
    private long idBook;

    private Date   noteDate;
    private int    noteValue;
    private String noteComment;

    private String firstName;
    private String lastName;
    private String userN;



    public Note() {}

    public Note(long idUser, long idBook, Date noteDate, int noteValue, String noteComment, String firstName, String lastName, String userN) {
        this.idUser = idUser;
        this.idBook = idBook;
        this.noteDate = noteDate;
        this.noteValue = noteValue;
        this.noteComment = noteComment;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userN = userN;
    }

    public Note(long idUser, long idBook, int noteValue, String noteComment) {
        this.idUser = idUser;
        this.idBook = idBook;
        this.noteValue = noteValue;
        this.noteComment = noteComment;
    }

    public long getIdUser() {
        return idUser;
    }
    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public long getIdBook() {
        return idBook;
    }
    public void setIdBook(long idBook) {
        this.idBook = idBook;
    }

    public Date getNoteDate() {
        return noteDate;
    }
    public void setNoteDate(Date noteDate) {
        this.noteDate = noteDate;
    }

    public int getNoteValue() {
        return noteValue;
    }
    public void setNoteValue(int noteValue) {
        this.noteValue = noteValue;
    }

    public String getNoteComment() {
        return noteComment;
    }
    public void setNoteComment(String noteComment) {
        this.noteComment = noteComment;
    }


    public String getUserN() {
        return userN;
    }

    public void setUserN(String userN) {
        this.userN = userN;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
