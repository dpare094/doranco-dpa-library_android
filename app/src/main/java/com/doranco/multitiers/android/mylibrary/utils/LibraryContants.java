package com.doranco.multitiers.android.mylibrary.utils;

public final class LibraryContants {

    public static final String EXTRA_USERNAME = "username";

    public static final String USERNAME_PATTERN = "^[a-zA-Z0-9_-]{6,15}$"; // les caractères  avec la taille {min-max} et fin $
    //comme notre appli library  public static final String PASSWORD_PATTERN = "^[a-zA-Z0-9_]*$"; // * signifie qu'il peut y avoir plusieurs fois ces caratères
    public static final String PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$";

    //public static final String ROOT_URL = "http://localhost:8080/library-web/webapi";
    public static final String ROOT_URL = "http://10.0.0.107:8080/library-web/webapi";
 //   public static final String ROOT_URL = "https://10.0.0.107:8182/library-web/webapi";

    public static final String RESPONSE_HEADER_MESSAGE = "Server message";

    public static final String BOOK_ID = "Book id";
    public static final String BOOK_TITLE = "Book Title";


    public static final String NOTEVALUE_PATTERN = "[1-5]";
}