package com.doranco.multitiers.android.mylibrary.rest;

import com.doranco.multitiers.android.mylibrary.model.Book;
import com.doranco.multitiers.android.mylibrary.model.Note;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;

import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

import dalvik.annotation.TestTargetClass;

@Rest(rootUrl = LibraryContants.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
public interface BookClient  extends RestClientErrorHandling {

    @Get("/books/allBooks")
    ResponseEntity <List<Book>> getAllBooks ();

    @Get("/books/notes/{id}")
    ResponseEntity <List<Note>> getBooksNotes(@Path long id);
}
