package com.doranco.multitiers.android.mylibrary.model;

public class User extends Identifier {

    private String  userName;
    private String  firstName;
    private String  lastName;
    private boolean admin;
    private boolean superAdmin;
    private String  password;

    /**
     *
     */
    public User() { }
    public User(String firstName, String lastName, String userName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        admin = false;
        superAdmin = false;
    }

    /**
     *
     */
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {return admin;}
    public boolean isSuperAdmin() {  return superAdmin; }

}
