package com.doranco.multitiers.android.mylibrary.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.model.Note;
import com.doranco.multitiers.android.mylibrary.sharedPref.LibraryPrefs_;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;
import com.doranco.multitiers.android.mylibrary.utils.LibraryUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import static java.util.regex.Pattern.matches;

@EActivity
public class AddNoteAcitivity extends AppCompatActivity {

    private static  final    String  LOG_TAG = "AddNoteAcitivity";

    @Bean
    protected LibraryUtils lbUtils;
    @Pref
    protected LibraryPrefs_ prefs;

    private static String mBookTitle;
    private static long mBookId;
    private int mNoteValue;

    @ViewById(R.id.add_note_my_book)
    protected TextView mAddNoteTitle;
    @ViewById(R.id.add_note_toolbar)
    protected Toolbar mToolbar;

    @ViewById(R.id.add_note_value)
    protected TextView mAddNoteValueView;
    @ViewById(R.id.add_note_comment)
    protected AutoCompleteTextView mAddNoteCommentView;


//    @ViewById(R.id.add_note_star1)  protected   Button mAddStar1View;
//    @ViewById(R.id.add_note_star2)  protected   Button mAddStar2View;
//    @ViewById(R.id.add_note_star3)  protected   Button mAddStar3View;
//    @ViewById(R.id.add_note_star4)  protected   Button mAddStar4View;
//    @ViewById(R.id.add_note_star5)  protected   Button mAddStar5View;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        setSupportActionBar(mToolbar);


        mBookTitle = this.getIntent().getStringExtra(LibraryContants.BOOK_TITLE);
        mBookId = Long.parseLong(this.getIntent().getStringExtra(LibraryContants.BOOK_ID));

        mAddNoteTitle.setText(mBookTitle);

        mAddNoteCommentView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptAddNote();
                    return true;
                }
                return false;
            }
        });
    }


    @Click(R.id.add_note_button)
    protected void attemptAddNote() {

        mAddNoteValueView.setError(null);
        mAddNoteCommentView.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (isNoteValueValid(mAddNoteValueView.getText().toString())) {

            mNoteValue = Integer.parseInt(mAddNoteValueView.getText().toString());

            Note myNote = new Note(prefs.idUser().get(), mBookId, mNoteValue, mAddNoteCommentView.getText().toString());

            if (TextUtils.isEmpty(mAddNoteValueView.getText().toString())) {
                mAddNoteValueView.setError(getString(R.string.error_field_required));
                focusView = mAddNoteValueView;
                cancel = true;
            }

            if (TextUtils.isEmpty(myNote.getNoteComment())) {
                mAddNoteCommentView.setError(getString(R.string.error_field_required));
                focusView = mAddNoteCommentView;
                cancel = true;
            }
        }
        else {
            mAddNoteValueView.setError(getString(R.string.error_field_neednumber_1_5));
            focusView = mAddNoteValueView;
            cancel = true;
        }


    }



    @Click(R.id.add_note_star1)
    protected void modifNote1() {
        mNoteValue = 1;
        showStars();

    }
    @Click(R.id.add_note_star2)
    protected void modifNote2() {
        mNoteValue = 2;
        showStars();

    }
    @Click(R.id.add_note_star3)
    protected void modifNote3() {
        mNoteValue = 3;
        showStars();

    }
    @Click(R.id.add_note_star4)
    protected void modifNote4() {
        mNoteValue = 4;
        showStars();

    }
    @Click(R.id.add_note_star5)
    protected void modifNote5() {
        mNoteValue = 5;
        showStars();

    }


    private void showStars() {

        if (mNoteValue >= 1)
            this.findViewById(R.id.add_note_star1).setBackground(this.getDrawable(R.drawable.star));
        else
            this.findViewById(R.id.add_note_star1).setBackground(this.getDrawable(R.drawable.star_grey));
        if (mNoteValue>= 2)
            this.findViewById(R.id.add_note_star2).setBackground(this.getDrawable(R.drawable.star));
        else
            this.findViewById(R.id.add_note_star2).setBackground(this.getDrawable(R.drawable.star_grey));
        if (mNoteValue>= 3)
            this.findViewById(R.id.add_note_star3).setBackground(this.getDrawable(R.drawable.star));
        else
            this.findViewById(R.id.add_note_star3).setBackground(this.getDrawable(R.drawable.star_grey));
        if (mNoteValue >= 4)
            this.findViewById(R.id.add_note_star4).setBackground(this.getDrawable(R.drawable.star));
        else
            this.findViewById(R.id.add_note_star4).setBackground(this.getDrawable(R.drawable.star_grey));
        if (mNoteValue >= 5)
            this.findViewById(R.id.add_note_star5).setBackground(this.getDrawable(R.drawable.star));
        else
            this.findViewById(R.id.add_note_star5).setBackground(this.getDrawable(R.drawable.star_grey));

        mAddNoteValueView.setText(Integer.toString(mNoteValue));

    }


        private boolean isNoteValueValid(String note) {
        return matches(LibraryContants.NOTEVALUE_PATTERN, note);
    }



}


