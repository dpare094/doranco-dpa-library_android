package com.doranco.multitiers.android.mylibrary.rest.errorHandler;

import android.content.Context;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.sharedPref.LibraryPrefs_;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.api.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;


@EBean
public class ErrorHandler  implements RestErrorHandler {

    @RootContext
    Context context;
    @Pref
    protected LibraryPrefs_ prefs;
    private String serverMessage;

    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {

        if (e instanceof ResourceAccessException) {
            serverMessage = context.getResources().getString(R.string.network_acces_error);
        }
        else {
            // notre exception specifique
            HttpClientErrorException httpException = (HttpClientErrorException) e;
            serverMessage = httpException.getResponseHeaders().getFirst(LibraryContants.RESPONSE_HEADER_MESSAGE);

        }
        prefs.serverMessage().put(serverMessage);
    }
}
