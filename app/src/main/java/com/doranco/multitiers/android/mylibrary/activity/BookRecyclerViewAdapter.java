package com.doranco.multitiers.android.mylibrary.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.model.Book;
import com.doranco.multitiers.android.mylibrary.rest.BookClient;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;


import org.androidannotations.annotations.Click;

import java.util.List;


/**
 * {@link RecyclerView.Adapter} that can display a {@link Book}
 * TODO: Replace the implementation with code for your data type.
 */

public class BookRecyclerViewAdapter extends RecyclerView.Adapter {


    private static  final    String  LOG_TAG = "BookRecyclerViewAdapter";

    private final List<Book> mValues;
    private Context context;
    private BookClient mBookClient;
    private Activity mActivity;

    private String mBookTitle;
    private long mBookId;

    private static final int BOOK_LIST_POS = 0;
    private static final int BOOK_LIST_TITLE_TYPE = 0;
    private static final int BOOK_ITEM_TYPE = 1;


     public BookRecyclerViewAdapter(List<Book> items) {
        mValues = items;
    }
//    public BookRecyclerViewAdapter(List<Book> items,  Context ctx) {
//        mValues = items;
//        context = ctx;
//    }
    public BookRecyclerViewAdapter(List<Book> items, Context ctx, Activity activity) {
        mValues = items;
        context = ctx;
        mActivity = activity;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == BOOK_LIST_POS)
            return BOOK_LIST_TITLE_TYPE;
        else
            return BOOK_ITEM_TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == BOOK_LIST_TITLE_TYPE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.book_list_title_item, parent, false);
            return new BookListTitleViewHolder(view);

        } else {

            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.book_item, parent, false);
            final BookItemViewHolder mViewHolder = new BookItemViewHolder(view);
            return  mViewHolder;
 //           return new BookItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        // mettre des variables final  pour pouvoir les prendre avec les boutons
        final int mPosition = position;

        if (position == BOOK_LIST_POS) {

           BookListTitleViewHolder bookListTitleViewHolder = (BookListTitleViewHolder) holder;
           bookListTitleViewHolder.mBookNumberView.setText(Integer.valueOf(mValues.size()).toString());

           bookListTitleViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO do something upon list title click
                }
            });
        } else {
            BookItemViewHolder bookItemViewHolder = (BookItemViewHolder) holder;
            // test pas necessaire, sert juste pour le deplacement du titre
            if (position < BOOK_LIST_POS) {
                bookItemViewHolder.mTitleView.setText(mValues.get(position).getTitle());
            }
            else {

                Book myBook = mValues.get(position - 1);
                bookItemViewHolder.mTitleView.setText(myBook.getTitle());

                //              String preface = "Une petite intro pour le livre '"+mValues.get(position-1).getTitle()+"' d'un auteur non moins célèbre.";

                String preface = myBook.getPreface();
                bookItemViewHolder.mPrefaceView.setText(preface);

                // affichage des images
                bookItemViewHolder.mImageView.setImageResource(context.getResources().getIdentifier(myBook.getImage(), "drawable", context.getPackageName()));

                //if ((mValues.get(position-1).getContent() == null) || (mValues.get(position-1).getContent().equals("")))
                // affichage des bouton

                if (myBook.getContent().length == 0)
                    bookItemViewHolder.mButtonBorrowView.setVisibility(View.GONE);
                else bookItemViewHolder.mButtonBorrowView.setVisibility(View.VISIBLE);
                if (myBook.getMean() > 0) {
                    bookItemViewHolder.mButtonAddNoteView.setVisibility(View.GONE);
                    bookItemViewHolder.mButtonLearnMoreView.setVisibility(View.VISIBLE);
                }
                else {
                    bookItemViewHolder.mButtonAddNoteView.setVisibility(View.VISIBLE);
                    bookItemViewHolder.mButtonLearnMoreView.setVisibility(View.GONE);
                }


                // affichage des notes Version 1 avec passage de la note dans le Book (non persistent)
//                if (myBook.getAverageNote()>=1)
//                    bookItemViewHolder.mBookStar1.setImageResource(R.drawable.star);
//                else
//                    bookItemViewHolder.mBookStar1.setImageResource(R.drawable.star_grey);
//                if (myBook.getAverageNote()>=2)
//                    bookItemViewHolder.mBookStar2.setImageResource(R.drawable.star);
//                else
//                    bookItemViewHolder.mBookStar2.setImageResource(R.drawable.star_grey);
//                if (myBook.getAverageNote()>=3)
//                    bookItemViewHolder.mBookStar3.setImageResource(R.drawable.star);
//                else
//                    bookItemViewHolder.mBookStar3.setImageResource(R.drawable.star_grey);
//                if (myBook.getAverageNote()>=4)
//                    bookItemViewHolder.mBookStar4.setImageResource(R.drawable.star);
//                else
//                    bookItemViewHolder.mBookStar4.setImageResource(R.drawable.star_grey);
//                if (myBook.getAverageNote()>=5)
//                    bookItemViewHolder.mBookStar5.setImageResource(R.drawable.star);
//                else
//                    bookItemViewHolder.mBookStar5.setImageResource(R.drawable.star_grey);


                // Version 2 récupereation des notes sans modif au niveau serveur pour average
                Double myMean = myBook.getMean();
                View noteView = bookItemViewHolder.mBookLinearStarView;

                if (myMean >= 1d)
                    noteView.findViewById(R.id.book_star1).setBackground(mActivity.getDrawable(R.drawable.star));
                else
                    noteView.findViewById(R.id.book_star1).setBackground(mActivity.getDrawable(R.drawable.star_grey));
                if (myMean >= 2d)
                    noteView.findViewById(R.id.book_star2).setBackground(mActivity.getDrawable(R.drawable.star));
                else
                    noteView.findViewById(R.id.book_star2).setBackground(mActivity.getDrawable(R.drawable.star_grey));
                if (myMean >= 3d)
                    noteView.findViewById(R.id.book_star3).setBackground(mActivity.getDrawable(R.drawable.star));
                else
                    noteView.findViewById(R.id.book_star3).setBackground(mActivity.getDrawable(R.drawable.star_grey));
                if (myMean >= 4d)
                    noteView.findViewById(R.id.book_star4).setBackground(mActivity.getDrawable(R.drawable.star));
                else
                    noteView.findViewById(R.id.book_star4).setBackground(mActivity.getDrawable(R.drawable.star_grey));
                if (myMean >= 5d)
                    noteView.findViewById(R.id.book_star5).setBackground(mActivity.getDrawable(R.drawable.star));
                else
                    noteView.findViewById(R.id.book_star5).setBackground(mActivity.getDrawable(R.drawable.star_grey));


            }

             bookItemViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent;
//                    intent = new Intent(mActivity, NoteListActivity_.class);
//                    mActivity.startActivity(intent);
                }

             });

            bookItemViewHolder.mButtonLearnMoreView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent;
      Log.d(LOG_TAG, "BOOK : ->" + mValues.get(mPosition - 1).getTitle());
                    intent = new Intent(mActivity, NoteListActivity_.class);
                    intent.putExtra(LibraryContants.BOOK_TITLE, mValues.get(mPosition - 1).getTitle() );
                    intent.putExtra(LibraryContants.BOOK_ID, Long.toString(mValues.get(mPosition - 1).getId()) );
                    mActivity.startActivity(intent);

                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return 1 + mValues.size();
    }

    public class BookListTitleViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mBookNumberView;


        public BookListTitleViewHolder(View view) {
            super(view);
            mView = view;
            mBookNumberView = (TextView) view.findViewById(R.id.book_list_title_number);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mBookNumberView.getText() + "'";
        }
    }

    public class BookItemViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleView;
        public final TextView mPrefaceView;
        public final ImageView mImageView;

        public final Button mButtonBorrowView;
        public final Button mButtonLearnMoreView;
        public final Button mButtonAddNoteView;


        // pour average methode 1
        public final ImageView mBookStar1;
        public final ImageView mBookStar2;
        public final ImageView mBookStar3;
        public final ImageView mBookStar4;
        public final ImageView mBookStar5;
        // pour average methode 2
        public final LinearLayout mBookLinearStarView;




        public BookItemViewHolder(View view) {
            super(view);
            mView = view;
            mTitleView =  view.findViewById(R.id.book_title);
            mPrefaceView =  view.findViewById(R.id.book_preface);
            mImageView = view.findViewById(R.id.book_image);
            mButtonBorrowView =  view.findViewById(R.id.book_button_borrow);
            mButtonLearnMoreView =  view.findViewById(R.id.book_button_learn_more);
            mButtonAddNoteView =  view.findViewById(R.id.book_button_add_note);

            // pour methode 1
            mBookStar1 = view .findViewById(R.id.book_star1);
            mBookStar2 = view .findViewById(R.id.book_star2);
            mBookStar3 = view .findViewById(R.id.book_star3);
            mBookStar4 = view .findViewById(R.id.book_star4);
            mBookStar5 = view .findViewById(R.id.book_star5);

            // pour methode 2
            mBookLinearStarView = view .findViewById(R.id.book_linear_star);


        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitleView.getText() + "'";
        }
    }


}
