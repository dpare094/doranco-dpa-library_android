package com.doranco.multitiers.android.mylibrary.activity;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.model.Credential;
import com.doranco.multitiers.android.mylibrary.model.User;
import com.doranco.multitiers.android.mylibrary.rest.UserClient;
import com.doranco.multitiers.android.mylibrary.rest.errorHandler.ErrorHandler;
import com.doranco.multitiers.android.mylibrary.sharedPref.LibraryPrefs_;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;
import com.doranco.multitiers.android.mylibrary.utils.LibraryUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.ResponseEntity;

/**
 * A login screen that offers login via email/password.
 */
@EActivity
public class LoginActivity extends AppCompatActivity  {

    private static  final    String  LOG_TAG = "LoginActivity";

    // UI references.
    @ViewById(R.id.username)       protected AutoCompleteTextView mUserNameView;
    @ViewById(R.id.password)       protected EditText mPasswordView;

    @ViewById(R.id.login_progress) protected ProgressBar mProgressView;
    @ViewById(R.id.login_form)     protected View mLoginFormView;

    @ViewById(R.id.my_login_toolbar) protected Toolbar mToolbar;


    @Bean  protected LibraryUtils lbUtils;
    @Pref  protected LibraryPrefs_ prefs;

    // Service REST
    @RestService  UserClient mUserClient;
    @Bean ErrorHandler mUserErrorHandler;
    @AfterInject void afterInject() {
        mUserClient.setRestErrorHandler(mUserErrorHandler);
    }

    private ResponseEntity mResponse;
    private ResponseEntity mResponseIdUser;


    private Boolean mLoginSuccess = false; // indique si le login est correct

    // on prefere les parametres global pour eviter de passer des paramètre au methode annotées
    private String mUserName;
    private String mPassword;

    private Pattern pattern;
    private Matcher matcher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // affichage de la toolbar
        setSupportActionBar(mToolbar);

        // Set up the login form.

// DAP remplacé par @ViewById        
//        mUserNameView = (AutoCompleteTextView) findViewById(R.id.username);
//        populateAutoComplete();
//
//        mPasswordView = (EditText) findViewById(R.id.password);


        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        // DAP remplacé par autre code sur on click
//        Button mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
//        mEmailSignInButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                attemptLogin();
//            }
//        });

// DAP remplacé par @ViewById          
//        mLoginFormView = findViewById(R.id.login_form);
//        mProgressView = findViewById(R.id.login_progress);

    }

// DAP demander la permission explicite des utilisateurs
// pas necessaire pour l'instant
//
//    private boolean mayRequestContacts() {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            return true;
//        }
//        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
//            return true;
//        }
//        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
//            Snackbar.make(mUserNameView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
//                    .setAction(android.R.string.ok, new View.OnClickListener() {
//                        @Override
//                        @TargetApi(Build.VERSION_CODES.M)
//                        public void onClick(View v) {
//                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
//                        }
//                    });
//        } else {
//            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
//        }
//        return false;
//    }

    /**
     * Callback received when a permissions request has been completed.
     */
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        if (requestCode == REQUEST_READ_CONTACTS) {
//            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                populateAutoComplete();
//            }
//        }
//    }

        /**
         *
         */
    @Click(R.id.sign_in_button)
    protected void attemptLogin() {

        // Reset errors.
        mUserNameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mUserName = mUserNameView.getText().toString();
        mPassword = mPasswordView.getText().toString();

        boolean cancel = false; // indicateur permettant d'appler doLogin ou pas
        View focusView = null;  // permet de deplacer le curseur sur un champ particulier

        // Check for a valid password, if the user entered one.
// DAP
//        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
//            mPasswordView.setError(getString(R.string.error_invalid_password));
//            focusView = mPasswordView;
//            cancel = true;
//        }

        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }
//        else if (!isPasswordValid(mPassword)) {
//            mPasswordView.setError(getString(R.string.error_invalid_password));
//            focusView = mPasswordView;
//            cancel = true;
//        }


        // Check for a valid mUserName
        if (TextUtils.isEmpty(mUserName)) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        }
//        else if (!isUserNameValid(mUserName)) {
//            mUserNameView.setError(getString(R.string.error_invalid_username));
//            focusView = mUserNameView;
//            cancel = true;
//        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            lbUtils.showProgress(true, this, mProgressView, mLoginFormView);
            doLogin();

        }
    }

    // la methode est protected car elle est annotée
    // on crée un autre thread (par background) dans le cas des plantages ou des traitement tres long
    // permet de ne pas figé l'application ou bloque l'utilisateur
    @Background
    protected void doLogin() {

        mLoginSuccess = false;

 /** Ancienne methode
       try {
            // Simulate network access.    // a enlever qd acces a la bd
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            mLoginSuccess = false;
        }

        // Cas ou on prend une base d'utilisateur
        for (User user : lbUtils.getDummyUsers_()) {
            if (user.getUserName().equals(mUserName))
                mLoginSuccess = user.getPassword().equals(mPassword);   // Account exists, return true if the password matches.
        }
*/

        mResponse = mUserClient.userAuthentification(new Credential(mUserName, mPassword));
        if (mResponse != null ) {
            mLoginSuccess = true;
        }

        onPostLogin();
    }

    // ce tread est adapte pour modifié interface graphique -- car on fait un showProgress non dans le thread principal
    @UiThread
    protected void onPostLogin() {
        if (mLoginSuccess) {
            // on l'ajoute dans sharedPrefs
            prefs.loggedUserName().put(mUserName);
            String headerToken = mResponse.getHeaders().getAuthorization();
//DAP recup idUser
            User user = (User)mResponse.getBody();
            prefs.idUser().put(user.getId());

            prefs.token().put(headerToken);
            // recup de l'id client



            Log.d(LOG_TAG, "---------------->login token : "+ prefs.loggedUserName().get()+ " // " + mUserName + "///" + prefs.idUser().get());
            //et on va dans la liste des livres
            startActivity(new Intent(this, BookListActivity_.class));

        } else {
           // on affiche plus le snackbar car il s'affiche maintenant dans le UserErrorHandler
            Snackbar.make(mLoginFormView, prefs.serverMessage().get(), Snackbar.LENGTH_SHORT).show();
           // Snackbar.make(findViewById(R.id.login_form), R.string.invalid_username_or_password, Snackbar.LENGTH_SHORT).show();
            mUserNameView.requestFocus();
        }
        // on cache la progressBar dans tous les cas car gere le cas de retour de resume
        lbUtils.showProgress(false, this, mProgressView, mLoginFormView);

    }

//    private boolean isUserNameValid(String userName) {
//        return matches(LibraryContants.USERNAME_PATTERN, userName);
//    }
//
//    private boolean isPasswordValid(String password) {
//        return matches(LibraryContants.PASSWORD_PATTERN, password);
//    }
//
//    private boolean matches ( String patternString, String stringToMatch ) {
//        pattern = Pattern.compile(patternString);
//        matcher = pattern.matcher(stringToMatch);
//        return matcher.matches();
//    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
// DAP plus necessaire
//    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
//
//        private final String mEmail;
//        private final String mPassword;
//
//        UserLoginTask(String email, String password) {
//            mEmail = email;
//            mPassword = password;
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... params) {
//            // TODO: attempt authentication against a network service.
//
//            try {
//                // Simulate network access.
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                return false;
//            }
//
//            for (String credential : DUMMY_CREDENTIALS) {
//                String[] pieces = credential.split(":");
//                if (pieces[0].equals(mEmail)) {
//                    // Account exists, return true if the password matches.
//                    return pieces[1].equals(mPassword);
//                }
//            }
//
//            // TODO: register the new account here.
//            return true;
//        }

//        @Override
//        protected void onPostExecute(final Boolean success) {
//            mAuthTask = null;
//            showProgress(false);
//
//            if (success) {
//                finish();
//            } else {
//                mPasswordView.setError(getString(R.string.error_incorrect_password));
//                mPasswordView.requestFocus();
//            }
//        }
//
//        @Override
//        protected void onCancelled() {
//            mAuthTask = null;
//            showProgress(false);
//        }
//    }

    /**
     *
     */
    @Click(R.id.goto_register_button)
    protected void startRegisterActivity() {

        Intent intent =new Intent(this, RegisterActivity_.class);
        intent.putExtra(LibraryContants.EXTRA_USERNAME, mUserNameView.getText().toString());
        startActivity(intent);

    }


}

