package com.doranco.multitiers.android.mylibrary.model;

public class Book extends Identifier {

    private String isbn;
    private String title;
    private String image;
    private String preface;
    private byte[] content;

    private long averageNote;
    private Double mean;

    /**
     *
     */
    public Book() { }
    public Book(String ISBN, String title, String image, String preface, byte[] content, long note) {
        this.isbn = ISBN;
        this.title = title;
        this.image = image;
        this.preface = preface;
        this.content = content;
        this.averageNote = note;
    }
    public Book(String ISBN, String title, String image, String preface) {
        this.isbn = ISBN;
        this.title = title;
        this.image = image;
        this.preface = preface;
    }
    public Book(String ISBN, String title, String image) {
        this.isbn = ISBN;
        this.title = title;
        this.image = image;
    }


    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPreface() {
        return preface;
    }

    public void setPreface(String preface) {
        this.preface = preface;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public long getAverageNote() {
        return averageNote;
    }

    public void setAverageNote(long averageNote) {
        this.averageNote = averageNote;
    }

    public Double getMean() {
        return mean;
    }
    public void setMean(Double mean) {
        this.mean = mean;
    }
}
