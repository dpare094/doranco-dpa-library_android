package com.doranco.multitiers.android.mylibrary.rest.errorHandler;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.activity.LoginActivity_;
import com.doranco.multitiers.android.mylibrary.sharedPref.LibraryPrefs_;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.api.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

/**
 * PLUS UTILISE
 */
@EBean
public class LoginErrorHandler   implements RestErrorHandler {

    @Pref
    protected LibraryPrefs_ prefs;


    //    @RootContext
//    Context context;
//
    private String serverMessage;
//    private View view;

    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {

//        view = ((LoginActivity_) context).findViewById (R.id.login_form);
//
        if (e instanceof ResourceAccessException) {
//            displayErrorMessage(view,context.getResources().getString(R.string.network_acces_error));

        }
        else {
            // notre exception specifique
            HttpClientErrorException httpException = (HttpClientErrorException) e;
            serverMessage = httpException.getResponseHeaders().getFirst(LibraryContants.RESPONSE_HEADER_MESSAGE);
 //            displayErrorMessage(view, serverMessage);

        }
        prefs.serverMessage().put(serverMessage);

    }

    // Affichage de la snackbar
    private void  displayErrorMessage (View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();

    }

}
