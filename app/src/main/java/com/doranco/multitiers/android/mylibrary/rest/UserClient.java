package com.doranco.multitiers.android.mylibrary.rest;

import com.doranco.multitiers.android.mylibrary.model.Credential;
import com.doranco.multitiers.android.mylibrary.model.User;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;

import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;


@Rest(rootUrl = LibraryContants.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
public interface UserClient extends RestClientErrorHandling {

    @Post("/users/login")
    ResponseEntity<User> userAuthentification(@Body Credential login); // @body permet de dire que l'objet credential est passé en paramètre

    @Post ("/users")
    ResponseEntity<User> newUser (@Body User user);

 //   @Get("/users/username/{username}")
 //   ResponseEntity getUserByUserName(@Path String username);


}
