package com.doranco.multitiers.android.mylibrary.model;

import java.io.Serializable;

/**
 * Created by philippe on 17/08/2018.
 */

public class Identifier implements Serializable {

    private long id;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
}
