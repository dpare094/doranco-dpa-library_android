package com.doranco.multitiers.android.mylibrary.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.model.Book;
import com.doranco.multitiers.android.mylibrary.model.Note;
import com.doranco.multitiers.android.mylibrary.rest.BookClient;
import com.doranco.multitiers.android.mylibrary.rest.errorHandler.ErrorHandler;
import com.doranco.multitiers.android.mylibrary.sharedPref.LibraryPrefs_;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;
import com.doranco.multitiers.android.mylibrary.utils.LibraryUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

@EActivity
public class NoteListActivity extends AppCompatActivity {

    @Bean   protected LibraryUtils lbUtils;
    @Pref   protected LibraryPrefs_ prefs;


    @ViewById(R.id.note_book_title)  protected TextView mNoteBookTitle;
    @ViewById(R.id.goto_add_note_button)  protected Button mNoteButtonGotoAddNote;
    @ViewById(R.id.goto_upgrade_note_button) protected  Button mNoteButtonGotoUpgradeNote;

    @ViewById(R.id.note_list_progress) protected ProgressBar mProgressView;
    @ViewById(R.id.note_linear_layout) protected LinearLayout mNoteListLinear;
    @ViewById(R.id.note_list_rv)  protected RecyclerView  mNoteRecyclerView;


    private RecyclerView.LayoutManager mLayoutManager;
    private NoteRecyclerViewAdapter mAdapter;
    private ActionBar mActionBar;

    private Boolean userAsNoteForBook = false;

    private static String mBookTitle;
    private static long mBookId;

    private List<Note> mNotes = new ArrayList<Note>();
    private List<Note> mSavedNotes;
    @RestService  BookClient mNoteClient;
    @Bean   ErrorHandler mNoteErrorHandler;
    @AfterInject
    void afterInject() {
        mNoteClient.setRestErrorHandler(mNoteErrorHandler);
    }
    private ResponseEntity<List<Note>> mResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);

        // on peut aussi la recup par @ViewById
        Toolbar toolbar = (Toolbar) findViewById(R.id.note_list_toolbar);
        setSupportActionBar(toolbar);

        // Enable the Up button  KO
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);

        mBookTitle =  this.getIntent().getStringExtra(LibraryContants.BOOK_TITLE);
        mBookId =  Long.parseLong(this.getIntent().getStringExtra(LibraryContants.BOOK_ID));

        // on change le titre en fonction du titre du livre choisi
        mNoteBookTitle.setText(mBookTitle);

        // affichage du bouton si pas de note deja en place -- ou changement du text

        //
        //
        //
        lbUtils.showProgress(true, this, mProgressView, mNoteListLinear);

        mLayoutManager = new LinearLayoutManager(this);
        mNoteRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new NoteRecyclerViewAdapter(mNotes, getApplicationContext(), this);
        mNoteRecyclerView.setAdapter(mAdapter);

        // on appelle le chargement des books dans un background
        doGetNotes();

    }

    @Background
    public void doGetNotes () {
        // on charge la base
        mResponse = mNoteClient.getBooksNotes((long)mBookId);
        if (mResponse != null) {
            mNotes.addAll(mResponse.getBody());



            mSavedNotes = new ArrayList<Note>();
            mSavedNotes.addAll(mNotes);

            onDataChanged(mSavedNotes);

        }
    }

    @UiThread
    public void onDataChanged(List<Note> newNotes) {

        mNotes.clear();
        mNotes.addAll(newNotes);
        mAdapter.notifyDataSetChanged();

        Long idUser =prefs.idUser().get();
        if ( prefs.idUser().exists() ) {
           userAsNoteForBook = false;
           for (Note note : newNotes) {

               if (idUser == note.getIdUser())
                {
                    userAsNoteForBook = true;
                }
            }

            if (userAsNoteForBook) {
                mNoteButtonGotoAddNote.setVisibility(View.GONE);
                mNoteButtonGotoUpgradeNote.setVisibility(View.VISIBLE);
            } else {
                mNoteButtonGotoAddNote.setVisibility(View.VISIBLE);
                mNoteButtonGotoUpgradeNote.setVisibility(View.GONE);
            }
        }
        else
            {
                mNoteButtonGotoAddNote.setVisibility(View.INVISIBLE); // je la rend invisilbe et pas gone pour garder le m^me espace dans l'ecran
                mNoteButtonGotoUpgradeNote.setVisibility(View.GONE);
            }
        lbUtils.showProgress(false, this, mProgressView, mNoteListLinear);

    }

    @Click(R.id.goto_add_note_button)
    protected void startAddNoteActivity() {

        Intent intent =new Intent(this, AddNoteAcitivity_.class);
        intent.putExtra(LibraryContants.BOOK_TITLE, this.getIntent().getStringExtra(LibraryContants.BOOK_TITLE));
        intent.putExtra(LibraryContants.BOOK_ID, this.getIntent().getStringExtra(LibraryContants.BOOK_ID));
        startActivity(intent);

    }


}
