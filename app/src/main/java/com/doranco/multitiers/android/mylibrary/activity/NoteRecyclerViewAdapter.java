package com.doranco.multitiers.android.mylibrary.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.model.Note;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NoteRecyclerViewAdapter extends RecyclerView.Adapter {

    private static  final    String  LOG_TAG = "NoteRecyclerViewAdapter";

    private final List<Note> mValues;
    private Context context;
    private Activity mActivity;



    public NoteRecyclerViewAdapter(List<Note> items) {
        mValues = items;
    }

    public NoteRecyclerViewAdapter(List<Note> items, Context ctx, Activity activity) {
        mValues = items;
        context = ctx;
        mActivity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);

        return new NoteItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        NoteItemViewHolder noteItemViewHolder = (NoteItemViewHolder) viewHolder;

        final Note mNote = mValues.get(position);

        noteItemViewHolder.mCommentView.setText(mNote.getNoteComment());
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String reportDate = df.format(mNote.getNoteDate());
        noteItemViewHolder.mDateView.setText(reportDate);

         noteItemViewHolder.mUserNView.setText(mNote.getUserN()+ "/ ");
         noteItemViewHolder.mUserView.setText(mNote.getFirstName()+" . "+ mNote.getLastName());

        View noteView = noteItemViewHolder.mNoteLinearStarView;

        if (mNote.getNoteValue() >= 1d)
            noteView.findViewById(R.id.note_star1).setBackground(mActivity.getDrawable(R.drawable.star));
        else
            noteView.findViewById(R.id.note_star1).setBackground(mActivity.getDrawable(R.drawable.star_grey));
        if (mNote.getNoteValue() >= 2d)
            noteView.findViewById(R.id.note_star2).setBackground(mActivity.getDrawable(R.drawable.star));
        else
            noteView.findViewById(R.id.note_star2).setBackground(mActivity.getDrawable(R.drawable.star_grey));
        if (mNote.getNoteValue() >= 3d)
            noteView.findViewById(R.id.note_star3).setBackground(mActivity.getDrawable(R.drawable.star));
        else
            noteView.findViewById(R.id.note_star3).setBackground(mActivity.getDrawable(R.drawable.star_grey));
        if (mNote.getNoteValue() >= 4d)
            noteView.findViewById(R.id.note_star4).setBackground(mActivity.getDrawable(R.drawable.star));
        else
            noteView.findViewById(R.id.note_star4).setBackground(mActivity.getDrawable(R.drawable.star_grey));
        if (mNote.getNoteValue() >= 5d)
            noteView.findViewById(R.id.note_star5).setBackground(mActivity.getDrawable(R.drawable.star));
        else
            noteView.findViewById(R.id.note_star5).setBackground(mActivity.getDrawable(R.drawable.star_grey));


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    // DAP
    // AJOUT SPECIFIQUE
    //
    public class NoteItemViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public final TextView mNoteView;
        public final TextView mDateView;
        public final TextView mCommentView;
        public final TextView mUserView;
        public final TextView mUserNView;

        public final LinearLayout mNoteLinearStarView;




        public NoteItemViewHolder(View view) {
            super(view);
            mView = view;

            mNoteView = view.findViewById(R.id.note_value);
            mDateView = view.findViewById(R.id.note_date);
            mCommentView = view.findViewById(R.id.note_comment);

            mNoteLinearStarView = view .findViewById(R.id.note_linear_star);
            mUserView = view.findViewById(R.id.note_userName);
            mUserNView = view.findViewById(R.id.note_userN);

        }
    }
 }
