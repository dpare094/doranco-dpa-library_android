package com.doranco.multitiers.android.mylibrary.activity;


import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.doranco.multitiers.android.mylibrary.R;

import com.doranco.multitiers.android.mylibrary.model.Book;
import com.doranco.multitiers.android.mylibrary.rest.BookClient;
import com.doranco.multitiers.android.mylibrary.rest.errorHandler.ErrorHandler;
import com.doranco.multitiers.android.mylibrary.sharedPref.LibraryPrefs_;
import com.doranco.multitiers.android.mylibrary.utils.LibraryUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;


@EActivity
public class BookListActivity extends AppCompatActivity {

    // pour le log, mettre le nom de la classe dans une constante
    private static  final    String  LOG_TAG = "BookListActivity";


    @ViewById(R.id.book_list_rv)  protected RecyclerView mBookRecyclerView;
    @Bean    protected LibraryUtils lbUtils;
    @Pref    protected LibraryPrefs_ prefs;


    @ViewById (R.id.book_list_swiperefresh) protected SwipeRefreshLayout mSwipeRefreshLayout;

    @ViewById(R.id.book_list_progress) protected ProgressBar mProgressView;
    @ViewById(R.id.book_list_toolbar)     protected View mBookListFormView;


    // Service REST
    @RestService   BookClient mBookClient;
    @Bean  ErrorHandler mBookErrorHandler;
    @AfterInject
    void afterInject() {
        mBookClient.setRestErrorHandler(mBookErrorHandler);
    }
    private ResponseEntity <List<Book>> mResponse;

    private RecyclerView.LayoutManager mLayoutManager;
    private BookRecyclerViewAdapter mAdapter;

    private List<Book> mBooks = new ArrayList<Book>();
    private List<Book> savedBooksInitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        // on peut aussi la recup par @ViewById
        Toolbar toolbar = (Toolbar) findViewById(R.id.book_list_toolbar);
        setSupportActionBar(toolbar);

        lbUtils.showProgress(true, this, mProgressView, mBookListFormView);

        /*
           * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
           * performs a swipe-to-refresh gesture.
           */
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");
                        doRefreshBooks();
                    }
                }
        );


        /*
        * ca commence ici
        */
        mLayoutManager = new LinearLayoutManager(this);
        mBookRecyclerView.setLayoutManager(mLayoutManager);

        // Ici on creer le lien entre le viewAdapter et mBooks
     //   mAdapter = new BookRecyclerViewAdapter(mBooks, getApplicationContext());
        mAdapter = new BookRecyclerViewAdapter(mBooks, getApplicationContext(), this);
        mBookRecyclerView.setAdapter(mAdapter);

        // on appelle le chargement des books dans un background
        doGetBooks();

        /*
        * et ca fini la
        */
    }

     /**
     *
     */
    @Background
    public void doGetBooks () {

        // on charge la base
        mResponse = mBookClient.getAllBooks();

        if (mResponse != null) {

            mBooks.addAll(mResponse.getBody());
            //mBooks = lbUtils.getBOOKS();
            // on sauvegarde dans une liste initiale
            savedBooksInitial = new ArrayList<Book>();
            savedBooksInitial.addAll(mBooks);

            // on appelle le changement (UiThread)
            onDataChanged(savedBooksInitial);
        }
    }

    /**
     *
     */
    public void doRefreshBooks() {

//        try {
//            // Simulate network access.    // a enlever qd acces a la bd
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//         }

        // on reaffiche la liste initiale
        onDataChanged(savedBooksInitial);

        // Arret de l'affichage
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * AJOUT DES ICONES DE MENU
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.book_list_menu, menu);

       final MenuItem searchItem = menu.findItem(R.id.action_book_search);
       SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(LOG_TAG, "String saisie : " + query);

                List<Book> myBooks = new ArrayList<Book>();
                for (Book book : savedBooksInitial) {

                        if ((book.getTitle().contains(query)) || (book.getIsbn().contains(query))) {
                                 myBooks.add(book);
                        }
                    }

                // pour faire disparaitre le clavier
                searchItem.collapseActionView();
                // pour mettre a jour la liste
                onDataChanged(myBooks);

                // TODO appel de la recherche dans la base
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(LOG_TAG, "String en cour: " + newText);
                return true;
            }
       });

        // Defini the listener
        // avec de vieilles methodes
        MenuItemCompat.OnActionExpandListener expandListener = new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                onDataChanged(savedBooksInitial);

                return true;  // Return true to collapse action view
            }
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                 // Do something when expanded
                return true;  // Return true to expand action view
            }
        };
        // Get the MenuItem for the action item
        MenuItem actionMenuItem = menu.findItem(R.id.action_book_search);
        // Assign the listener to that action item
        MenuItemCompat.setOnActionExpandListener(actionMenuItem, expandListener);

         //return super.onCreateOptionsMenu(menu);

        return true;
    }


    @UiThread
    public void onDataChanged(List<Book> newBooks) {

        mBooks.clear();
        mBooks.addAll(newBooks);
        mAdapter.notifyDataSetChanged();

        lbUtils.showProgress(false, this, mProgressView, mBookListFormView);


    }
    /**
     * ACTIONS DES ICONES DE MENU
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
//            case R.id.action_book_search:
//                Log.d(LOG_TAG, "Action Book SEARCH");
//                return true;
            case R.id.action_book_refresh:
                Log.d(LOG_TAG, "Action Book REFRESH");
                doRefreshBooks();
                return true;
            case R.id.action_book_setting:
                Log.d(LOG_TAG, "Action Book SETTING");
                return true;
            case R.id.action_book_help:
                Log.d(LOG_TAG, "Action Book HELP");
              return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





}
