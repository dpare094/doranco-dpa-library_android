package com.doranco.multitiers.android.mylibrary.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;


import android.os.Build;
import android.view.View;

import android.widget.ProgressBar;

import com.doranco.multitiers.android.mylibrary.model.Book;
import com.doranco.multitiers.android.mylibrary.model.User;

import org.androidannotations.annotations.EBean;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collections;
import java.util.List;


/**
 *
 */
@EBean (scope = EBean.Scope.Singleton)
public class LibraryUtils {
// plus necessaire
  //  protected List<String> dummyUsers = new ArrayList<String>(Arrays.asList ("premier:Premier1","deuxieme:Deuxiem2"));
    protected  List<User>  dummyUsers_  =new ArrayList <> (Arrays.asList (
            new User(null, null, "premier", "Premier1" ),
            new User(null, null, "deuxieme", "Deuxiem2" ),
            new User(null, null, "a", "a" )));
    protected static List<String> books = new ArrayList<String>(Arrays.asList(
            "Et un de plus en moins",
            "Si ça continu il faut que ça cesse",
            "Vendredi 13",
            "En attendant l'année dernière",
            "Faut pas pousser mémé dans les orties",
            "Il fait beau et chaud, ou l'inverse",
            "JoeBar Team",
            "Zut le voilà",
            "Dalt Wisney"
    ));
    protected static List<String> images = new ArrayList<String>(Arrays.asList(
            "image","image","mignon2",
            "image2","image2","mignon1",
            "mignon1","mignon2","samplebookimage"
    ));


    /**
             *
             */

//    public void setDummyUsers(List<String> dummyUsers) {
//        this.dummyUsers = dummyUsers;
//    }
//    public List<String> getDummyUsers() {
//        return dummyUsers;
//    }
//
//    public void addUser (String user) {
//        this.dummyUsers.add(user);
//    }

    public List<User> getDummyUsers_() { return dummyUsers_;  }
    public void setDummyUsers_(List<User> dummyUsers_) {this.dummyUsers_ = dummyUsers_; }

    public void addUser_ (User user) { this.dummyUsers_.add(user); }

    private static final List<Book> BOOKS = new ArrayList<Book>();
    private static final int COUNT = books.size();

    static {
        Collections.shuffle(images);
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyBook(i));
        }
    }

    private static void addItem(Book item) {
        BOOKS.add(item);
    }
    private static Book createDummyBook(int position) {
        Book book= null;
        if (position <= books.size()){
  //          book = new Book( "ISBN " + position,books.get(position-1));
//            book = new Book( "ISBN " + position,books.get(position-1), images.get((position-1)));
            book = new Book( "ISBN " + position,books.get(position-1), images.get((position-1)),"Une petite intro pour le livre '"+books.get(position-1)+"' d'un auteur non moins célèbre.");
        }
        else {
 //           book = new Book( "ISBN " + position, "Un livre avec un ISBN "+position);
            book = new Book( "ISBN " + position, "Un livre avec un ISBN "+position, "");
        }

        return book;
    }

    public static List<Book> getBOOKS() {
        return BOOKS;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show, Activity callingActivity, final ProgressBar progressViewToShowOrHide, final View viewToShowOrHide ) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = callingActivity.getResources().getInteger(android.R.integer.config_shortAnimTime);

            viewToShowOrHide.setVisibility(show ? View.GONE : View.VISIBLE);
            viewToShowOrHide.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    viewToShowOrHide.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressViewToShowOrHide.setVisibility(show ? View.VISIBLE : View.GONE);
            progressViewToShowOrHide.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressViewToShowOrHide.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressViewToShowOrHide.setVisibility(show ? View.VISIBLE : View.GONE);
            viewToShowOrHide.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
