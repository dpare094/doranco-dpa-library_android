package com.doranco.multitiers.android.mylibrary.activity;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.model.User;
import com.doranco.multitiers.android.mylibrary.rest.UserClient;
import com.doranco.multitiers.android.mylibrary.rest.errorHandler.ErrorHandler;
import com.doranco.multitiers.android.mylibrary.sharedPref.LibraryPrefs_;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;
import com.doranco.multitiers.android.mylibrary.utils.LibraryUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.ResponseEntity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@EActivity
public class RegisterActivity extends AppCompatActivity {

    @ViewById(R.id.fisrtname)  protected AutoCompleteTextView mFirstNameView;
    @ViewById(R.id.lastname)   protected AutoCompleteTextView mLastNameView;

    @ViewById(R.id.username)   protected AutoCompleteTextView mUserNameView;
    @ViewById(R.id.password)   protected EditText mPasswordView;
    @ViewById(R.id.repeated_password)  protected EditText mRepeatedPasswordView;

    @Bean protected LibraryUtils lbUtils;
    @Pref protected LibraryPrefs_ prefs;

    @ViewById(R.id.register_progress) protected ProgressBar mProgressView;
    @ViewById(R.id.register_form)     protected View mRegisterFormView;

    @ViewById(R.id.my_register_toolbar) protected Toolbar mToolbar;


    // Service REST
    @RestService     UserClient mUserClient;
//    @Bean    ErrorHandler mUserErrorHandler;
// on appel d'abord LoginActivity dans lequel setRestErrorHandler est déja appeler avec mUserclient donc pas besoin de la remettre
//    @AfterInject
//    void afterInject() {
//        mUserClient.setRestErrorHandler(mUserErrorHandler);
//    }
    private ResponseEntity mResponse;

    private ActionBar mActionBar;

    private User   mUser;
    private String mUserName;
    private String mRepeatedPassword;

    private Boolean mRegisterSuccess = false;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // affichage de la toolbar
        setSupportActionBar(mToolbar);



        // on peut agir sur la barre de titre
        // Get a support ActionBar corresponding to this toolbar
        mActionBar = getSupportActionBar();
        // Enable the Up button
        mActionBar.setDisplayHomeAsUpEnabled(true);


        //mActionBar.setTitle(R.string.action_borrow);

        // getIntent().getExtras() retourne un objet Bundle
//        mUserName = this.getIntent().getExtras().getString(LibraryContants.EXTRA_USERNAME);
        mUserName =  this.getIntent().getStringExtra(LibraryContants.EXTRA_USERNAME);

        if ((!mUserName.equals(null)) && (!mUserName.equals("")))
            mUserNameView.setText(mUserName);

        mUser = new User();

        // Gestion d'appui sur la touche ENTER
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });
        mRepeatedPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });


    }


    @Click(R.id.register_button)
    protected void attemptRegister() {

        mFirstNameView.setError(null);
        mLastNameView.setError(null);
        mUserNameView.setError(null);
        mPasswordView.setError(null);
        mRepeatedPasswordView.setError(null);

        boolean cancel = false;
        View focusView = null;

        // On recupere les infos du User
        mUser = new User(mFirstNameView.getText().toString(), mLastNameView.getText().toString(), mUserNameView.getText().toString(), mPasswordView.getText().toString());
        mRepeatedPassword = mRepeatedPasswordView.getText().toString();

        // Controles de saisie
        if (TextUtils.isEmpty(mRepeatedPassword)) {
            mRepeatedPasswordView.setError(getString(R.string.error_field_required));
            focusView = mRepeatedPasswordView;
            cancel = true;
        } else if (!mRepeatedPassword.equals(mUser.getPassword())) {
            mRepeatedPasswordView.setError(getString(R.string.error_repeated_password_not_matching));
            focusView = mRepeatedPasswordView;
            cancel = true;
        }

        if  (TextUtils.isEmpty(mUser.getPassword())) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(mUser.getPassword())) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if  (TextUtils.isEmpty(mUser.getUserName())) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        } else if (!isUserNameValid(mUser.getUserName())) {
            mUserNameView.setError(getString(R.string.error_invalid_username));
            focusView = mUserNameView;
            cancel = true;
        }

        if  (TextUtils.isEmpty(mUser.getLastName())) {
            mLastNameView.setError(getString(R.string.error_field_required));
            focusView = mLastNameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(mUser.getFirstName())) {
            mFirstNameView.setError(getString(R.string.error_field_required));
            focusView = mFirstNameView;
            cancel = true;
        }
        // en cas d'erreur on se place sur un champs
        if (cancel) {
            focusView.requestFocus();
        } else {
            lbUtils.showProgress(true, this, mProgressView, mRegisterFormView);
            doRegister();
        }

    }

    @Background
    protected void doRegister () {

        mRegisterSuccess = false;

//        try {
//            // Simulate network access.    // a enlever qd acces a la bd
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            mRegisterSuccess = false;
//        }
//
//        // TODO faire l'insertion dans la base de donnée
//        // Enregistrement du User
//        lbUtils.addUser_(mUser);
//        //lbUtils.addUser(mUser.getUserName() + ":" + mUser.getPassword()); // plus necessaire si on prend un utilisateur
//        // TODO si tout est ok alors mRegisterSuccess = true sinon false
//        mRegisterSuccess = true;

        mResponse = mUserClient.newUser(mUser);
        if (mResponse != null)
            mRegisterSuccess = true;

        // Et on passe à la suite si tout va bien
        onPostRegistration();

    }

    @UiThread
    protected void onPostRegistration () {

        if (mRegisterSuccess) {
            // On retourne sur l'activité
            // Pour tester je vais sur le login voir si le user est bien enregistré
             startActivity(new Intent(this, LoginActivity_.class));
        } else {
            // TODO si l'enregistrement en base est KO, mettre un message
      //      Snackbar.make(mRegisterFormView, R.string.error_register_user, Snackbar.LENGTH_SHORT).show();
            Snackbar.make(mRegisterFormView, prefs.serverMessage().get(), Snackbar.LENGTH_SHORT).show();
            mFirstNameView.requestFocus();
        }
        // on cache la progressBar dans tous les cas car gere le cas de retour de resume
        lbUtils.showProgress(false, this, mProgressView, mRegisterFormView);
    }

    /**
    *
    */
    private boolean isUserNameValid(String userName) {
        return matches(LibraryContants.USERNAME_PATTERN, userName);
    }

    private boolean isPasswordValid(String password) {
        return matches(LibraryContants.PASSWORD_PATTERN, password);
    }

    private boolean matches ( String patternString, String stringToMatch ) {
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher  = pattern.matcher(stringToMatch);
        return matcher.matches();
    }

}
