package com.doranco.multitiers.android.mylibrary.activity;


import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.doranco.multitiers.android.mylibrary.R;
import com.doranco.multitiers.android.mylibrary.sharedPref.LibraryPrefs_;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

@EActivity
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.main_list_toolbar) protected Toolbar mToolbar;
    @Pref   protected LibraryPrefs_ lb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar(mToolbar);


        // si l'utilsateur existe   on envoi sur l'activité de livres
        //                             sinon sur l'activité de login
//   DAP ajout des boutons pour eviter de tapper le loggin - A remettre
        Intent intent;
        if (lb.loggedUserName().exists())
            intent = new Intent(this, BookListActivity_.class);
        else
           intent = new Intent(this, LoginActivity_.class);

        startActivity(intent);

        // pour effacer l'activité une fois passer vers l'activité suivante
        //this.finish();
    }

    @Click(R.id.main_book_button)
    protected void startGoToBook() {

        startActivity(new Intent(this,BookListActivity_.class));

    }
    @Click(R.id.main_connect_button)
    protected void startGoToLogin() {

        startActivity(new Intent(this,LoginActivity_.class));

    }


    // le menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // les actions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_to_book:
                startGoToBook();
                return true;
            case R.id.action_to_login:
                startGoToLogin();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
