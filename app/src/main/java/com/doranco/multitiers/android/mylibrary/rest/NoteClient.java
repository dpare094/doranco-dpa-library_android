package com.doranco.multitiers.android.mylibrary.rest;

import com.doranco.multitiers.android.mylibrary.model.Note;
import com.doranco.multitiers.android.mylibrary.utils.LibraryContants;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

@Rest(rootUrl = LibraryContants.ROOT_URL, converters = {MappingJackson2HttpMessageConverter.class})
public interface NoteClient extends RestClientErrorHandling {

    @Get("/notes/notesByBook/{id}")
    ResponseEntity getNotesByBook (@Path long id);

}
